from flask import Flask
from sqlalchemy_wrapper import SQLAlchemy
from user.views import user_blueprint

app = Flask(__name__)
db = SQLAlchemy('sqlite:///test1.db')

app.register_blueprint(user_blueprint)
app.config['SECRET_KEY'] = 'thisissecret'

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)
