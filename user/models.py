from flask_sqlalchemy import SQLAlchemy

from sqlalchemy_wrapper import SQLAlchemy

db = SQLAlchemy('sqlite:///user_database.db')


class User(db.Model):
    __tablename__ = 'user'

    id = db.Column(db.Integer, primary_key=True)
    uname = db.Column(db.String)
    first_name = db.Column(db.String)
    last_name = db.Column(db.String)
    gender = db.Column(db.String)
    reg_Number = db.Column(db.String)
    email = db.Column(db.String)
    pas1 = db.Column(db.String)




db.create_all()