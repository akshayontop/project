from flask import session

from user.models import User, db


class Register_User(object):

    def __init__(self):
        pass

    def add_user(self, user_details):
        ll = []
        user_one = User(first_name=user_details['first_name'], last_name=user_details['user_name'],
                        email=user_details['email'], reg_Number=user_details['id'], pas1=user_details['password'],
                        uname=user_details['user_name'], gender=user_details['gender'])
        db.session.add(user_one)
        db.session.commit()
        # session['user'] = user_details['first_name']
        # print session['user']
        ll.append(True)
        ll.append(user_one.first_name)
        print ll
        return ll

    def check_user(self, user_details):
        user_one = db.session.query(User).filter_by(email=user_details['username']).first()
        if user_one is None:
            return None
        elif user_one.pas1 == user_details['password']:
            session['user'] = user_details['username']
            print session['user']
            return user_one.first_name

        return None

    def check_session_exitst(self):
        if session['user'] is not None:
            return True
        else:
            return False

    def logout_user(self):
        session.pop('user', None)
        return True
