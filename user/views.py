from flask import request, jsonify, Blueprint, render_template, url_for
from werkzeug.utils import redirect

from user.register import Register_User
from jinja2 import TemplateNotFound

user_blueprint = Blueprint("user", __name__, url_prefix='/', template_folder='../templates')


@user_blueprint.route('/')
def index():
    try:
        return render_template('login.html')
    except TemplateNotFound:
        return jsonify({"message": "Not Found"}), 400


@user_blueprint.route('/signup')
def index2():
    try:
        return render_template('signup.html')
    except TemplateNotFound:
        return jsonify({"message": "Connection Lost"}), 400


@user_blueprint.route('/login', methods=['POST'])
def login():
    form_data = request.form
    print request.form
    form_fields = ('username', 'password')
    if not all([form_data.get(form_data_entry) != None for form_data_entry in form_fields]):
        return jsonify({"message": "All Fields are Required!"}), 400
    else:
        r1 = Register_User()
        user_details = {'username': form_data.get('username'), 'password': form_data.get('password')}
        status = r1.check_user(user_details)
        print "LOG IN", status
    if status is not None:
        return render_template('homepage.html', result=status)
        # return jsonify({'message': 'login Success'}),200

    error = 'Invalid credentials'
    return render_template('login.html', error=error)


@user_blueprint.route('/signup_process', methods=['POST'])
def signup_process():
    form_data = request.form
    print request.form
    form_fields = ('first_name', 'last_name', 'gender', 'user_name', 'password', 'email', 'id')

    for i in form_fields:
        if form_data.get(i) is None:
            return jsonify({"message": i}), 400

    if not all([form_data.get(form_data_entry) != None for form_data_entry in form_fields]):
        return jsonify({"message": "All Fields are Required!"}), 400
    else:
        user_details = {'first_name': form_data.get('first_name'), 'last_name': form_data.get('last_name'),
                        'id': form_data.get('id'), 'user_name': form_data.get('user_name'),
                        'email': form_data.get('email'), 'gender': form_data.get('gender'),
                        'password': form_data.get('password')}
        r1 = Register_User()
        status = r1.add_user(user_details)
        if status[0]:
            return render_template('homepage.html', result=status[1])

    return jsonify({"message": "Connection Lost"}), 400


@user_blueprint.route('/homepage')
def homepage():
    return render_template('homepage.html')


@user_blueprint.route('/logout')
def logout():
    r1 = Register_User()
    status = r1.logout_user()
    if status:
        return redirect('http://localhost:5000')

    return redirect('http://localhost:5000')
