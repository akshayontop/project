# Login Service

This project has been developed as a Login Micro-service.

### Project Setup

In order to setup project, we've to do three steps

  - Install Docker 
  - Pull webappdemo (Docker Image)
  - Run Docker File

### Install Docker

Execute below mentioned Commands 


```sh

$#!/usr/bin/env bash

$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
$ sudo apt-get install software-properties-common python-software-properties
$ sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
$ sudo apt-get update
$ apt-cache policy docker-ce
$ sudo apt-get install -y docker-ce docker-compose
$ sudo usermod -aG docker ${USER}
$ sudo apt-get install git

```
### Pull Docker Image File

```sh
$  docker pull akshayontop/webappdemo:part1

```
### Execute Docker
```sh
$  docker run -p 5000:5000 akshayontop/webappdemo:part1 
```
This will start Server on *http://localhost:5000/* 

Executing above command will start the Login service.
